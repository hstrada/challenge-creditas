const images = {
  logo: require('../assets/imgs/logo.png'),
  github: require('../assets/imgs/github.png'),
  view: require('../assets/imgs/view.png'),
};

export default images;

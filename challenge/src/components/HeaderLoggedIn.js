import React from 'react';
import {Image, View} from 'react-native';
import images from 'res/images';

const Header = () => {
  return (
    <View style={{display: 'flex', flexDirection: 'row'}}>
      <Image source={images.logo} style={{width: 50, height: 50}} />
      <Image source={images.github} style={{width: 150, height: 50, resizeMode: 'contain'}} />
    </View>
  );
};

export default Header;

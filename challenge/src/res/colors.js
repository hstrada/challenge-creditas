const colors = {
  primary: '#1b7',
  secondary: '#053B25',
  white: '#FFFFFF',
  gray: '#CCC',
  black: '#000',
  primaryDark: '#0B7A4E',
  primaryLighter: '#12C77F',
  error: '#bb1111',
  warning: '#ffcc00',
  bgColor: '#F6F6F6',
};

export default colors;

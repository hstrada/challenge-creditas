﻿# Challenge - Creditas

## Como foi a solução pensada?

Antes de iniciar a construção do aplicativo, eu levantei o que precisava ser feito e inclui minhas atividades no trello (link disponível abaixo). 
À partir do momento de ter minhas atividades a serem feitas, eu analisei a API do GitHub e qual era o fluxo pensado para a aplicação. Uma vez que o layout estava definido, decidi seguí-lo e focar na parte lógica.
Uma vez que eu realizei os testes na API do GitHub (utilizando o Postman), eu utilizei o draw.io para verificar as condições.

Devido ao tempo, eu optei por fazer o fluxo base da aplicação. 
Criei um componente stateless e outro stateful a fim de apresentar os dois modelos, bem como estruturei o aplicativo a fim de mostrar a utilização do colors e styles, por exemplo.
Assim, pude mostrar em pequenas partes do código, como eu faria cada etapa. Optei por ser objetiva e mostrar suas pequenas utilizações e como eu estruturia cada pedaço.

Por exemplo, na primeira tela, eu não validei se o valor que o usuário incluiu era realmente um email. Eu validei apenas se era nulo.
Assim, eu pude mostrar por quê, por exemplo, os testes são importantes. Logo, incluí isto nos testes a fim de mostrar como eu faria uma utilização dele.

Independente do tempo de entrega, eu preferi mostrar um pouco de cada parte do desenvolvimento. Estilização (manual), componentização, requisições à API, testes, entre outros recursos. Estrutura do projeto.

Como eu gosto de desenvolver e gosto de encerrar ciclos, sei que a entrega é na segunda-feira, mas irei realizar todas as modificações que faltam e as melhorias que eu pensei em colocar no final do README.md.
Assim, eu sempre posso encerrar desafios, ciclos, e evoluir. Acredito que a tecnologia e o desenvolvimento há muito disso e também, eu posso melhorar e testar coisas novas. Como, por exemplo, o fast refresh. Nessa versão, ficou fantástico.

Independente do resultado, eu agradeço a oportunidade que me foi concedida e é claro, um feedback. Nos fazem crescer e nos engradecem.
Obrigada!

## Organizao
https://trello.com/b/WqlgluH4/challenge

## Estrutura
**Challenge** - contém o projeto do mobile criado
**Recursos** - contém a coleção do postman utilizada para testar os endpoints

### Endpoints Utilizados
https://api.github.com

### Recursos Utilizados

*npm install react-navigation
npm install react-native-reanimated react-native-gesture-handler react-native-screens@^1.0.0-alpha.23
npm install react-navigation-stack*


### Melhorias
- [ ] Verificar Conexao de Internet
- [X] Styled Components - https://www.styled-components.com/docs/basics
- [X] Adicionar ActivityIndicator no Login
- [ ] Adicionar ActivityIndicator na Lista de Repositorios
- [ ] Adicionar ActivityIndicator na Lista de Commits
- [ ] FlatList - Scroll Infinito
- [ ] SplashScreen para verificar se ja ha algum login feito previamente
- [X] Incluir ícone no aplicativo

### Exemplo
![](app.gif)

### Exemplo Nova Versão
![](app_v2.gif)
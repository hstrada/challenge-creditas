import React from 'react';
import styled from 'styled-components/native';

import colors from 'res/colors';

const CustomInput = props => (
  <TextInput
    underlineColorAndroid={colors.primary}
    placeholderTextColor={colors.gray}
    {...props}
  />
);

export default CustomInput;

const TextInput = styled.TextInput`
  width: 80%;
  margin: 15px;
  padding: 2px;
  height: 40;
`;

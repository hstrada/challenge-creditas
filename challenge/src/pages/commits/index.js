import React from 'react';
import {AsyncStorage} from 'react-native';

// componentes
import HeaderLoggedIn from 'components/HeaderLoggedIn';
import Container from 'components/Container';
import List from 'components/List';
import ListItemCommits from 'components/ListItemCommits';

import commitsMock from './commits.json';

import {getCommits} from './fetchCommits';

export default class CommitsScreen extends React.Component {
  state = {
    repository: null,
    commits: [],
  };

  componentDidMount() {
    const repositorieName = JSON.stringify(
      this.props.navigation.getParam('repositorio', '404'),
    );
    // this.setState({commits: commitsMock});
    this._fetchCommits(repositorieName.replace(/\"/g, ''));
  }

  _fetchCommits = async name => {
    this.setState({repository: name});
    this.setState({
      token: await AsyncStorage.getItem('@challenge:token'),
    });
    this.setState({
      username: await AsyncStorage.getItem('@challenge:username'),
    });

    await getCommits(this.state.username, this.state.token, name).then(res =>
      this.setState({commits: res}),
    );
  };

  render() {
    return (
      <Container>
        <HeaderLoggedIn />
        <List
          dados={this.state.commits}
          render={({item}) => (
            <ListItemCommits
              name={item.commit.author.name}
              img={item.author !== null ? item.author.avatar_url : null}
            />
          )}
          chave={({item}) => item.sha.toString()}
        />
      </Container>
    );
  }
}

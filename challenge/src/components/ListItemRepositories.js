import React from 'react';
import styled from 'styled-components/native';

import colors from 'res/colors';
import images from 'res/images';

const ListItem = props => (
  <Container>
    <ContainerItem>
      <Titulo>{props.name}</Titulo>
      <Texto>
        {props.description !== null
          ? props.description
          : 'Descrição Indisponível'}
      </Texto>
    </ContainerItem>
    <ContainerImagem>
      <Botao onPress={props.action}>
        <Imagem source={images.view} />
      </Botao>
    </ContainerImagem>
  </Container>
);

const Texto = styled.Text`
  font-size: 10;
  color: #999;
  line-height: 24;
`;

const Titulo = styled.Text`
  font-size: 14;
  color: #333;
`;

const Container = styled.View`
  flex-direction: row;
  border-bottom-width: 0.9;
  border-bottom-color: gray;
`;

const ContainerItem = styled.View`
  width: 90%;
  margin-top: 5;
`;

const ContainerImagem = styled.View`
  justify-content: center;
  align-content: center;
  align-items: center;
`;

const Botao = styled.TouchableOpacity``;

const Imagem = styled.Image`
  width: 22;
  height: 22;
  tint-color: ${colors.primaryDark};
`;

export default ListItem;

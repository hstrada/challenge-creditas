import constants from 'res/constants';

// https://api.github.com/repos/hstrada/AmigoSecreto/commits

export const getCommits = (username, token, repository) => {
  return fetch(
    constants.URIBASE + '/repos/' + username + '/' + repository + '/commits',
    {
      headers: {
        Authorization: token,
      },
    },
  ).then(res => res.json());
};

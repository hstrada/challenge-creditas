import base64 from '../../helpers/base64';
import constants from 'res/constants';

export const fetchUserFromCredentials = (username, password) => {
  let email = username.toLowerCase().trim();

  return fetch(constants.URIBASE + '/user', {
    headers: {
      Authorization: 'Basic ' + base64.btoa(email + ':' + password),
    },
  }).then(res => res.json());
};

import React from 'react';
import styled from 'styled-components/native';

import images from 'res/images';

const Header = () => <ImageRes source={images.logo} />;

export default Header;

const ImageRes = styled.Image`
  width: 50;
  height: 50;
`;

import React from 'react';
import styled from 'styled-components/native';

const ListItemCommits = props => (
  <Container>
    {props.img !== null ? (
      <Imagem source={{uri: props.img}} />
    ) : (
      <Texto>Sem Imagem</Texto>
    )}
    <Texto>{props.name !== undefined ? props.name : 'Sem Nome'}</Texto>
  </Container>
);

const Texto = styled.Text`
  width: 70%;
`;

const Container = styled.View`
  text-align: center;
  justify-content: center;
  align-content: center;
  align-items: center;
  width: 100%;
  flex-direction: row;
`;

const Imagem = styled.Image`
  width: 15%;
  height: 50;
`;

export default ListItemCommits;

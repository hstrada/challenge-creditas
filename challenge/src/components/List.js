import React from 'react';
import styled from 'styled-components/native';

const List = props => (
  <CustomList
    data={props.dados}
    renderItem={props.render}
    ListEmptyComponent={<CustomText>Não há itens.</CustomText>}
    keyExtractor={item => props.chave}
  />
);

export default List;

const CustomList = styled.FlatList`
  width: 80%;
`;

const CustomText = styled.Text``;

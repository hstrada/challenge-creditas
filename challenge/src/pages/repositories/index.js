import React, {Component} from 'react';
import {AsyncStorage} from 'react-native';

// informacoes
import repos from './repos.json';
import {getRepositories} from './fetchRepositories';

// componentes
import Container from 'components/Container';
import HeaderLoggedIn from 'components/HeaderLoggedIn';
import CustomInput from 'components/CustomInput';
import List from 'components/List';
import ListItemRepositories from 'components/ListItemRepositories';

export default class RepositoriesScreen extends Component {
  constructor() {
    super();
    this.state = {
      token: null,
      username: null,
      dados: [],
      initial: [],
    };
  }

  componentDidMount() {
    this._recoverUser();
  }

  _recoverUser = async () => {
    this.setState({
      token: await AsyncStorage.getItem('@challenge:token'),
    });
    this.setState({
      username: await AsyncStorage.getItem('@challenge:username'),
    });
    this._findRepositories();
  };

  _findRepositories = async () => {
    // this.setState({dados: repos, initial: repos});
    await getRepositories(this.state.username, this.state.token).then(res =>
      this.setState({dados: res, initial: res}),
    );
  };

  _filter = e => {
    let text = e.toLowerCase();
    let filteredList = this.state.dados;
    let filteredName = filteredList.filter(item => {
      return item.name.toLowerCase().match(text);
    });
    if (!text || text === '') {
      this.setState({
        dados: this.state.initial,
      });
    } else if (Array.isArray(filteredName)) {
      this.setState({
        dados: filteredName,
      });
    }
  };

  _onPress = item => {
    this.props.navigation.navigate('Commits', {repositorio: item});
  };

  render() {
    return (
      <Container>
        <HeaderLoggedIn />
        <CustomInput
          placeholder="Filtrar por Nome do Repositório"
          onChangeText={e => this._filter(e)}
        />
        <List
          dados={this.state.dados}
          render={({item}) => (
            <ListItemRepositories
              name={item.name}
              description={item.description}
              action={() => this._onPress(item.name)}
            />
          )}
          chave={({item}) => item.id.toString()}
        />
      </Container>
    );
  }
}

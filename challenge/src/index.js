import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import SignInScreen from './pages/signin/';
import AuthScreen from './pages/auth/';
import RepositoriesScreen from './pages/repositories/';
import CommitsScreen from './pages/commits/';

// authorization stack
const LoginStackNavigator = createStackNavigator(
  {
    SignIn: SignInScreen,
    Auth: AuthScreen,
  },
  {
    initialRouteName: 'SignIn',
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    },
  },
);

// app navigator stack
const AppStackNavigator = createStackNavigator(
  {
    Repositories: RepositoriesScreen,
    Commits: CommitsScreen,
  },
  {
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    },
  },
);

export default createAppContainer(
  // createSwitchNavigator({AppStackNavigator, LoginStackNavigator}),
  createSwitchNavigator({LoginStackNavigator, AppStackNavigator}),
);

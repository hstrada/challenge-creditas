import React from 'react';
import styled from 'styled-components/native';

import colors from 'res/colors';

const CustomText = props => <Texto type={props.type}>{props.info}</Texto>;

const Texto = styled.Text`
  width: 80%;
  margin: 5px;
  color: ${props =>
    (props.type === 'primary' && colors.primary) ||
    (props.type === 'danger' && colors.error) ||
    (props.type === 'warning' && colors.warning) ||
    colors.black};
  text-align: center;
`;

export default CustomText;

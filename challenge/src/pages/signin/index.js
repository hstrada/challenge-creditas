import React from 'react';

// componentes
import Container from 'components/Container';
import Header from 'components/Header';
import CustomButton from 'components/CustomButton';
import CustomInput from 'components/CustomInput';
import CustomText from 'components/CustomText';

export default class SignInScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      email: null,
      msgErro: null,
    };
  }

  _searchEmail = () => {
    // apenas valida que o email não pode ser nulo
    if (this.state.email === null || this.state.email === '') {
      this.setState({msgErro: 'O e-mail não pode ser nulo.'});
      return false;
    } else {
      this.setState({msgErro: null});
    }
    // envia como parametro o email para a proxima tela
    this.props.navigation.navigate('Auth', {
      email: this.state.email,
    });
  };

  render() {
    return (
      <Container>
        <Header />
        <CustomInput
          placeholder="Digite o E-mail"
          onChangeText={email => this.setState({email})}
          value={this.state.email}
        />
        {!!this.state.msgErro && (
          <CustomText info={this.state.msgErro} type="warning" />
        )}
        <CustomButton action={this._searchEmail} text="Next" />
      </Container>
    );
  }
}

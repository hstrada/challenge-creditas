import React from 'react';
import styled from 'styled-components/native';

import colors from 'res/colors';

const Loading = () => <Charging color={colors.primary} size="small" />;

export default Loading;

const Charging = styled.ActivityIndicator``;

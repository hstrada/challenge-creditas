import styled from 'styled-components/native';

import colors from 'res/colors';

const Container = styled.View`
  flex: 1;
  background-color: ${colors.bgColor};
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: center;
  align-items: center;
  align-content: center;
`;

export default Container;

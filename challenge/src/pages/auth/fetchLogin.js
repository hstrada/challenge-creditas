import base64 from '../../helpers/base64';
import constants from 'res/constants';

export const getUserAndPassword = (username, password) => {
  let email = username.toLowerCase().trim();

  return fetch(constants.URIBASE, {
    headers: {
      Authorization: 'Basic ' + base64.btoa(email + ':' + password),
    },
  }).then(res => res.json());
};

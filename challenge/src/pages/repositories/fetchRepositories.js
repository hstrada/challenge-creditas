import constants from 'res/constants';

// https://api.github.com/users/hstrada/repos

export const getRepositories = (username, token) => {
  return fetch(constants.URIBASE + '/users/' + username + '/repos', {
    headers: {
      Authorization: token,
    },
  }).then(res => res.json());
};

import React from 'react';
import {AsyncStorage} from 'react-native';

import base64 from '../../helpers/base64';

// requisicoes da api
import {getUserAndPassword} from './fetchLogin';
import {fetchUserFromCredentials} from './fetchUser';

// componentes
import Container from 'components/Container';
import Header from 'components/Header';
import CustomInput from 'components/CustomInput';
import CustomButton from 'components/CustomButton';
import CustomText from 'components/CustomText';
import Loading from 'components/Loading';

export default class SignInScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      email: null,
      password: null,
      msgErro: null,
      loading: false,
    };
  }

  componentDidMount() {
    const userEmail = JSON.stringify(
      this.props.navigation.getParam('email', 'Nenhum e-mail informado.'),
    );
    this._recoverEmail(userEmail.replace(/\"/g, ''));
  }

  _recoverEmail = userEmail => {
    this.setState({email: userEmail});
  };

  _searchEmailAndPassword = async () => {
    this.setState({loading: true, msgErro: null});
    await getUserAndPassword(this.state.email, this.state.password).then(
      res => {
        // caso as credenciais sejam invalidas, mostrar msg de erro
        if (res.message === 'Bad credentials') {
          this._showError();
        }
        // caso contrario, gravar as credenciais, buscar o nome do usuario, redirecionar para os repositorios
        else {
          this.setState({loading: false});
          this._saveAndRedirectUser();
        }
      },
    );
  };

  _saveAndRedirectUser = async () => {
    await AsyncStorage.setItem(
      '@challenge:token',
      'Basic ' + base64.btoa(this.state.email + ':' + this.state.password),
    );
    fetchUserFromCredentials(this.state.email, this.state.password).then(
      response => {
        AsyncStorage.setItem('@challenge:username', response.login);
        this.props.navigation.navigate('Repositories');
      },
    );
  };

  _showError = () => {
    this.setState({loading: false});
    this.setState({msgErro: 'Usuário ou Senha Inválidos'});
  };

  _goBack = () => {};

  render() {
    const {goBack} = this.props.navigation;
    return (
      <Container>
        <Header />
        <CustomText info={this.state.email} />
        <CustomInput
          value={this.state.password}
          placeholder="digite a senha"
          onChangeText={password => this.setState({password})}
          secureTextEntry={true}
        />
        {!!this.state.msgErro && (
          <CustomText info={this.state.msgErro} type="danger" />
        )}
        {this.state.loading && <Loading />}
        <CustomButton action={this._searchEmailAndPassword} text="Login" />
        <CustomButton action={() => goBack()} text="Back" />
      </Container>
    );
  }
}

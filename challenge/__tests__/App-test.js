import 'react-native';
import React from 'react';
import SignIn from '../src/pages/signin/index';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';

// it('check null for email', () => {
//   let input = renderer.create(<SignIn />).getInstance();
//   input.setState({email: 'e-mail'});
//   input._searchEmail();
//   expect(input.state.msgErro).toEqual(null);
// });

it('check if it is email', () => {
  let input = renderer.create(<SignIn />).getInstance();
  input.setState({email: 'a@admin.com'});
  // input.setState({email: 'admin'});

  let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (reg.test(input.state.email) === false) {
    input.setState({msgErro: false});
  } else {
    input.setState({msgErro: true});
  }
  input._searchEmail();
  expect(input.state.msgErro).toEqual(true);
});

import React from 'react';
import styled from 'styled-components/native';

import colors from 'res/colors';

const CustomButton = props => (
  <ButtonContainer onPress={props.action}>
    <ButtonText>{props.text}</ButtonText>
  </ButtonContainer>
);

export default CustomButton;

const ButtonContainer = styled.TouchableOpacity`
  height: 38;
  shadow-color: rgba(0,0,0, 0.4);
  shadow-offset: { height: 1, width: 1 };
  shadow-opacity: 1;
  shadow-radius: 1;
  elevation: 2;
  width: 80%;
  border-radius: 5;
  border-width: 0;
  border-color: ${colors.white};
  background-color: ${colors.white};
  justify-content: center;
  align-items: center;
  margin-top: 10;
`;

const ButtonText = styled.Text`
  font-size: 14px;
  color: ${colors.primary};
  text-transform: uppercase;
  text-align: center;
  letter-spacing: 4;
`;
